using System.Collections;
using System.Collections.Generic;
using Normal.Realtime;
using UnityEngine;

public class LightSwitch : MonoBehaviour
{
    private bool _isOn = false;
    [SerializeField] private LightController _light;
    [SerializeField] private Material _offMat;
    [SerializeField] private Material _onMat;
    private MeshRenderer _renderer;
    private bool _canActivate = true;
    void Start()
    {
        _renderer = GetComponent<MeshRenderer>();
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player") && _canActivate)
        {
            Debug.Log("activate light");
            _canActivate = false;
            TriggerLight();
            StartCoroutine(LightReuse());
        }
    }

    void Update()
    {
        //if (_isOn)
        //{
        //    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, 45);
        //}
        //else
        //{
        //    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, -45);
        //}
    }

    private IEnumerator LightReuse()
    {
        yield return new WaitForSeconds(1);
        _canActivate = true;
    }

    public void TriggerLight()
    {
        _isOn = !_isOn;
        _light.ActivateLight(_isOn);
        if (_isOn)
        {
            _renderer.material = _onMat;
        }
        else
        {
            _renderer.material = _offMat;
        }
        
    }

}
