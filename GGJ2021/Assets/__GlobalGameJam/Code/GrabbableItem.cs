using Normal.Realtime;
using UnityEngine;

public class GrabbableItem : MonoBehaviour
{
    private bool _grabbed;
    private string _holdingObject;
    private RealtimeTransform _realtimeTransform;
    private RealtimeView _realtimeView;
    [SerializeField] private Rigidbody _rigidBody;

    public bool IsGrabbed
    {
        get { return _grabbed; }
    }

    private void Awake()
    {
        _realtimeView = GetComponent<RealtimeView>();
        _realtimeTransform = GetComponent<RealtimeTransform>();
        _rigidBody = GetComponent<Rigidbody>();
    }

    public void Update()
    {
        if (_grabbed)
            if (_realtimeTransform != null)
            {
                _realtimeTransform.RequestOwnership();
                Debug.Log("requesting ownership");
            }
    }

    public void Grabbed()
    {
        _grabbed = true;
        Debug.Log("holding object:");
        _rigidBody.isKinematic = true;
    }

    public void Dropped()
    {
        Debug.Log("drop request:");
        _grabbed = false;
        _rigidBody.isKinematic = false;
    }
}