using System;
using System.Collections;
using System.Collections.Generic;
using Normal.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour
{
    [SerializeField] private bool _timerStarted;
    [SerializeField] private RealtimeAvatarManager _realTimeAvatarManager;
    [SerializeField] private List<TMP_Text> _clocks;
    [SerializeField] private int _maxPlayers = 4;
    [SerializeField] private bool _clockActivated = false;
    [SerializeField] private bool _gameStarted = false;
    [SerializeField] private double _gameTime = 600;
    [SerializeField] private int _clockDelay = 5;
    private double _currentTime;

    private int _roomCount;

    private bool _phase2Played = false;
    private bool _tikTokPlayed = false;
    private bool _oneMinutePlayed = false;
    private bool _doBetterPlayed = false;
    private bool _gettingWarmPlayed = false;
    private bool _tenSecondsPlayed = false;
    private bool _wellDonePlayed = false;
    private bool _byeByePlayed = false;

    public GameObject explosionVFX;

    // Start is called before the first frame update
    void Start()
    {
      AudioManager.instance.PlayAmbient();
      AudioManager.instance.PlayDialogue("OhYoureUp", 5);
      AudioManager.instance.PlayDialogue("PayClose", 10);
      AudioManager.instance.PlayDialogue("FeelingALittleLost", 20);
      AudioManager.instance.PlayDialogue("DontWorry", 26);
      AudioManager.instance.PlayDialogue("WereGoingToPlay", 35);
      AudioManager.instance.PlayDialogue("DoYouSee", 45);
      AudioManager.instance.PlayDialogue("ComeInHandy", 52);
      StartCoroutine(InitialExplosion());
      AudioManager.instance.PlayDialogue("Demolition", 57);
      //timer Starts, set in editor for this script (delay)
      AudioManager.instance.PlayDialogue("WorkTogether", 64);
      StartCoroutine(InitialExplosion());
    }

    // Update is called once per frame
    void Update()
    {
        _roomCount = _realTimeAvatarManager.avatars.Count;
        if ( _roomCount == _maxPlayers & !_gameStarted)
        {
            _gameStarted = true;
            // Spawn Players
            // Start Greeting
            StartCoroutine(CountDownToStart());
        }
        else if(_gameStarted && _clockActivated)
        {
            _currentTime += Time.deltaTime;
            var actualGameTime = TimeSpan.FromSeconds(_gameTime - _currentTime);

            foreach (var textbox in _clocks)
            {
                textbox.SetText($"{actualGameTime.Minutes}:{actualGameTime.Seconds}");
            }
            var actualGameTimeSec = (actualGameTime.Minutes * 60) + actualGameTime.Seconds;
            //Debug.Log(actualGameTimeSec);

            if ((actualGameTimeSec == 300) && !_tikTokPlayed)
            {
              _tikTokPlayed = true;
              AudioManager.instance.PlayDialogue("TikTok", 0);
            }
/*            else if (passedPhase2 && !_phase2Played)
            {
              _phase2Played = true;
              AudioManager.instance.PlayDialogue("WellDone1", 0);
            }*/
            else if ((actualGameTimeSec == 180) && !_doBetterPlayed)
            {
              _doBetterPlayed = true;
              AudioManager.instance.PlayDialogue("DoBetter", 0);
            }
      /*      else if ((actualGameTimeSec == 120) && !_explosions)
            {
              _oneMinutePlayed = true;
              AudioManager.instance.PlayDialogue("OneMinute", 0);
            }*/
            else if ((actualGameTimeSec == 60) && !_oneMinutePlayed)
            {
              _oneMinutePlayed = true;
              AudioManager.instance.PlayDialogue("OneMinute", 0);
            }
            else if ((actualGameTimeSec == 30) && !_gettingWarmPlayed)
            {
              _gettingWarmPlayed = true;
              AudioManager.instance.PlayDialogue("GettingWarm", 0);
            }
            else if ((actualGameTimeSec == 10) && !_tenSecondsPlayed)
            {
              _tenSecondsPlayed = true;
              AudioManager.instance.PlayDialogue("TenSeconds", 0);
            }
            else if ((actualGameTimeSec == 0) && !_byeByePlayed)// && !Escaped) // and not escaped
            {
              _byeByePlayed = true;
              AudioManager.instance.PlayDialogue("ByeBye", 0);
            }
/*            else if ((actualGameTimeSec == 0) && !_wellDonePlayed && Escaped) // and escaped
            {
              _wellDonePlayed = true;
              AudioManager.instance.PlayDialogue("WellDone2", 0);
            }
*/
        }
    }

    private IEnumerator CountDownToStart()
    {
        yield return new WaitForSeconds(_clockDelay);
        _clockActivated = true;
    }

    // where I left it was particle effect will not play if disabled and reenabled...
    // not sure how to "play" it after reactivating/reenabling
    // - and I deactivated it in the scene because otherwise the destory particle Script
    // removes it while it is idle...so maybe...try getting rid of destroy Script
    // turn of "on awake" and then see if it works (or why destroy script was needed)
    private IEnumerator InitialExplosion()
    {
      yield return new WaitForSeconds(55f);
      AudioManager.instance.PlaySound("Explosion1");
      //explosionVFX.SetActive(true);
      //explosionVFX.play();
      GameObject explosion = Instantiate(explosionVFX, transform.position, Quaternion.identity);
//      explosion.enabled = true;
      explosion.transform.localScale = new Vector3(1,1,1);
    }
}
