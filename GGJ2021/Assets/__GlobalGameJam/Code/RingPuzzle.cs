using System.Collections;
using System.Collections.Generic;
using Normal.Realtime;
using UnityEngine;

public class RingPuzzle : MonoBehaviour
{
    [SerializeField] private List<DropDoor> _dropDoors;
    [SerializeField] private GameObject _ringObject;
    [SerializeField] private GameObject _diamondObject;
    [SerializeField] private MeshRenderer _ringRenderer;
    [SerializeField] private MeshRenderer _diamondRenderer;
    [SerializeField] private bool _lightOn;
    [SerializeField] private bool _diamondInPlace = false;
    [SerializeField] private bool _ringInPlace = false;
    [SerializeField] private bool _doorDropsOpen = false;


    void OnTriggerStay(Collider col)
    {
        if (col.gameObject == _ringObject)
        {
            // Horrible Hack
            var grabbableItem = col.gameObject.GetComponent<GrabbableItem>();
            if (grabbableItem != null && !grabbableItem.IsGrabbed)
            {
                StartCoroutine(DropDelay(col.gameObject, _ringRenderer.gameObject));
                _ringRenderer.enabled = true;
                _ringInPlace = true;
            }


        }

        if (col.gameObject == _diamondObject)
        {
            // Horrible Hack
            var grabbableItem = col.gameObject.GetComponent<GrabbableItem>();
            if (grabbableItem != null && !grabbableItem.IsGrabbed)
            {
                StartCoroutine(DropDelay(col.gameObject, _diamondRenderer.gameObject));
                _diamondRenderer.enabled = true;
                _diamondInPlace = true;
            }
        }
    }

    void Update()
    {
        if (!_doorDropsOpen && (_diamondInPlace && _ringInPlace && _lightOn))
        {
            _doorDropsOpen = true;
            Debug.Log("DropBoxOpened");
        }
    }

    private IEnumerator DropDelay(GameObject go, GameObject parent)
    {
        yield return new WaitForEndOfFrame();
        go.SetActive(false);

    }

    public void LightActivated(bool lightState)
    {
        _lightOn = lightState;
    }

}
