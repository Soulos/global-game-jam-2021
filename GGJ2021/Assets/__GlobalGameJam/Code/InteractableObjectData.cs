using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Interactable Object Data", menuName = "Interactable Object Data")]
public class InteractableObjectData : ScriptableObject
{
  public new string name;
  public int health;
  public int weight;
}

public enum ObjectType
{
    Battery,
    Corkscrew,
    Stapler,
    Key,
}