using System.Collections.Generic;
using Normal.Realtime;
using Normal.Realtime.Serialization;
using UnityEngine;

public class SpawnPointScript : MonoBehaviour
{
    [SerializeField] private Realtime _realtime;

    private readonly RealtimeDictionary<SpawnPointModel>
        _realtimeDictionary = new RealtimeDictionary<SpawnPointModel>();

    [SerializeField] private List<Transform> _spawnPoints;

    public Vector3 GetSpawnPoint(int clientId)
    {
        return _spawnPoints[clientId].position;
        //var spawnPointId = -1;
        //foreach (var item in _realtimeDictionary)
        //{
        //    if (item.Value.clientId == clientId) return _spawnPoints[item.Value.spawnPoint].position;

        //    spawnPointId = item.Value.spawnPoint;
        //}

        
        //spawnPointId += 1;
        //_realtimeDictionary.Add((uint) spawnPointId,
        //    new SpawnPointModel {clientId = clientId, spawnPoint = spawnPointId});
        //Debug.Log($"SpawnPointId: {spawnPointId}");
        
    }
}