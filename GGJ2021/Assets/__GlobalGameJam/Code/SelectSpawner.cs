using System.Collections;
using System.Collections.Generic;
using Normal.Realtime;
using UnityEngine;

public class SelectSpawner : MonoBehaviour
{
    [SerializeField] private SpawnPointScript _spawnPoints;
    [SerializeField] private GameObject _playerRoot;
    private RealtimeAvatarManager _avatar;
    
    Vector3 _spawnPoint = Vector3.zero;
    private bool _spawned = false;
    private bool _spawning = false;
    private Realtime _realtime;
    // Start is called before the first frame update
    void Start()
    {
        _realtime = GetComponent<Realtime>();
        _avatar = GetComponent<RealtimeAvatarManager>();
        _realtime.didConnectToRoom += _realtime_didConnectToRoom;
    }

    private void _realtime_didConnectToRoom(Realtime realtime)
    {
        _spawnPoint = _spawnPoints.GetSpawnPoint(_realtime.clientID);
        Debug.Log($"newPos: {_spawnPoint}");
        _playerRoot.transform.position = _spawnPoint;
        _spawning = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (_realtime.connected && _spawning)
        {
            if (_playerRoot.transform.position != _spawnPoint)
            {
                Debug.Log("spawned");
                _spawning = false;
                _playerRoot.transform.position = _spawnPoint;
                
            }
        }
        
    }
}
