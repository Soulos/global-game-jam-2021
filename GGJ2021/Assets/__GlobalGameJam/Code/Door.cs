using System.Collections;
using System.Collections.Generic;
using Tilia.Interactions.Interactables.Interactables;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] private BoxCollider _boxCollider;

    void Start()
    {
        _boxCollider.enabled = false;
    }
    public void ActivateDoor()
    {
        _boxCollider.enabled = true;
    }
}
