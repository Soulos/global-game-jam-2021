using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PressurePad : MonoBehaviour
{
    private UnityAction PressurePadListener;
    public GameObject explosionVFX;

    void Awake()
    {
      PressurePadListener = new UnityAction (PressurePadBoom);  //***May need to put AnimatePeekOnly BELOW this
      AudioManager.instance.PlaySound("FallingDebris");
    }

    void OnEnable ()
    {
      EventManager.StartListening ("PressurePad", PressurePadListener);
    }

    void OnDisable ()
    {
      EventManager.StopListening ("PressurePad", PressurePadListener);
    }

    void PressurePadBoom ()
    {
      //execute the explosion
      GameObject explosion = Instantiate(explosionVFX, transform.position, Quaternion.identity);
      explosion.transform.localScale = new Vector3(1,1,1);
    }

  //triggered when object collides, but object will fall through
    void OnTriggerEnter (Collider whatTouched)
    {
      //check collider type
      if (whatTouched.gameObject.GetComponent<PuzzleItem>().data.name == "Corkscrew")
      {
        Debug.Log("Corkscrew!!! That's what we wanted!");
        EventManager.TriggerEvent("PressurePad");
        AudioManager.instance.PlaySound("Explosion1");
      }
      else
      {
        Debug.Log("Sorry, Pressure Pad was triggered by " + whatTouched + ", we don't want that");
      }
    }

    //for use when pressure pad has collider isTrigger off, object will not fall through
    void OnCollisionEnter (Collision collision)
    {
      Collider whatTouched = collision.collider;
      Debug.Log("Pressure Pad collided with " + whatTouched);
    }
}
