using System.Collections;
using System.Collections.Generic;
using Normal.Realtime;
using UnityEngine;

public class PadlockScript : MonoBehaviour
{
    [SerializeField] private Realtime _realTime;
    [SerializeField] private RealtimeView _realtimeView;
    [SerializeField] private GameObject _particalPrefab;
    [SerializeField] private Door _door;
    [SerializeField] private GameObject _requiredKey;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnTriggerStay(Collider col)
    {
        if (col.gameObject.name == _requiredKey.name)
        {
            RemovePadlock();
        }
    }

    void RemovePadlock()
    {
        Instantiate(_particalPrefab, gameObject.transform.position, Quaternion.identity);
        _door.ActivateDoor();
        Destroy(gameObject);

    }

}
