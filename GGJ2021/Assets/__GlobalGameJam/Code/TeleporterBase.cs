using System.Collections;
using System.Collections.Generic;
using Normal.Realtime;
using UnityEngine;

public class TeleporterBase : MonoBehaviour
{
    public LayerMask _layerMask;
    public Transform _particlePrefab;
    public Transform _targetLocation;
    // Start is called before the first frame update

    void OnTriggerStay(Collider col)
    {
        if ((_layerMask & 1 << col.gameObject.layer) == 1 << col.gameObject.layer)
        {
            
            var realtimeView = col.gameObject.GetComponent<RealtimeView>();
            if (realtimeView.realtime.connected)
            {
                realtimeView.RequestOwnership();
                Instantiate(_particlePrefab);
                StartCoroutine(Telport(col.gameObject));
            }
        }

    }

    IEnumerator Telport(GameObject go )
    {
        yield return new WaitForSeconds(1.5f);
        go.transform.position = _targetLocation.position;
    }

    
}
