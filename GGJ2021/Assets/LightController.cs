using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour
{
    private Light _light;
    public RingPuzzle _lightListener;
    void Start()
    {
        _light = GetComponent<Light>();
    }

   

    public void ActivateLight(bool isOn)
    {
        _light.enabled = isOn;
        _lightListener?.LightActivated(isOn);
        

    }
}
